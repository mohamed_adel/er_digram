﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ErDigram
{
    static class ApplicationManager
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static List<Connectors> connectors = new List<Connectors>();
        static List<Shape> Shapes = new List<Shape>();
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm());
        }
    }
}
