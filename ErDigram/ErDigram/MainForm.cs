﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ErDigram
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Shape s, p;
            s = new Entity(this);
            p = new Relation(this);
            s.ID = "2";
            p.ID = "3";
            Connectors c1 = new Connectors(s, p, this);
            c1.id = 123;
        }
    }
}
