﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ErDigram
{
    class Drag : Action
    {
        private Control activeControl;
        private Point previousLocation;
        public Drag(Control control)
        {
            control.MouseDown += new MouseEventHandler(MouseDown);
            control.MouseMove += new MouseEventHandler(MouseMove);
            control.MouseUp += new MouseEventHandler(MouseUp);
        }
        void MouseDown(object sender, MouseEventArgs e)
        {
            activeControl = sender as Control;
            previousLocation = e.Location;
            ////Cursor = Cursors.Hand;
        }

        void MouseMove(object sender, MouseEventArgs e)
        {
            if (activeControl == null || activeControl != sender)
                return;

            var location = activeControl.Location;
            location.Offset(e.Location.X - previousLocation.X, e.Location.Y - previousLocation.Y);
            activeControl.Location = location;
        }

        void MouseUp(object sender, MouseEventArgs e)
        {
            activeControl = null;
        }
    }
}
