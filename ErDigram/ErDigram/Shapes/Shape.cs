﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ErDigram
{
    class Shape
    {
        public string ID,Type;
        public Control control;
        public List<Connectors> connectors = new List<Connectors>();
        public Point connectionFromPoint()
        {

            return new Point(control.Location.X + (control.Size.Width / 2), control.Location.Y + control.Size.Height);
        }
        public Point connectionToPoint()
        {

            return new Point(control.Location.X + (control.Size.Width / 2), control.Location.Y + control.Size.Height);
        }
        protected void LocationChanged(object sender, EventArgs e)
        {
            for (int i = 0; i < connectors.Count; i++)
            {
                connectors[i].Update();
            }
        }
        protected string getType()
        {
            return Type;
        }
    }
}
