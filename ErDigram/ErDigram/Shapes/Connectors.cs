﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;
namespace ErDigram
{
    class Connectors
    {
        public Shape from, to;
        List<Line> Lines;
        public int id;
        MainForm form;
        public bool total;
        public Connectors(Shape _from, Shape _to, MainForm F)
        {
            total = false;
            Lines = new List<Line>();
            from = _from;
            to = _to;
            from.connectors.Add(this);
            to.connectors.Add(this);
            form = F;
            CreateListOfLines();
        }
        public void Update()
        {
            CreateListOfLines();
        }
        public void CreateListOfLines()
        {
            for (int i = 0; i < Lines.Count; i++)
            {
                form.Controls.Remove(Lines[i].shapeContainer);
            }
            Lines.Clear();
            Point Pfrom = from.connectionFromPoint();
            Point Pto = to.connectionToPoint();
            Lines.Add(new Line(Pfrom.X, Pfrom.Y, Pfrom.X, Pfrom.Y + 50, form, total));
            Lines.Add(new Line(Pfrom.X, Pfrom.Y + 50, Pto.X, Pfrom.Y + 50, form, total));
            Lines.Add(new Line(Pto.X, Pfrom.Y + 50, Pto.X, Pto.Y + 50, form, total));
            Lines.Add(new Line(Pto.X, Pto.Y + 50, Pto.X, Pto.Y, form, total));
        }
    }
}
