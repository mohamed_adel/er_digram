﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ErDigram
{
    class Entity : Shape
    {
        System.Windows.Forms.Label label;
        
        Drag drag;
        public Entity(MainForm F)
        {


            control=new System.Windows.Forms.Panel();
            control.Location = new System.Drawing.Point(100, 100);
            control.Size = new System.Drawing.Size(100, 100);
            control.DragDrop += new System.Windows.Forms.DragEventHandler(this.DragDrop);
            control.BackgroundImage = global::ErDigram.Properties.Resources.entity;
            control.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            control.LocationChanged += new System.EventHandler(this.LocationChanged);

            drag = new Drag(control);
            label = new System.Windows.Forms.Label();
            label.Text = "label1";
            label.Click += new System.EventHandler(this.OnClick);
            label.BackColor = System.Drawing.Color.Transparent;

            control.Controls.Add(label);
            F.Controls.Add(control);
        }
        private void OnClick(object sender, EventArgs e)
        {
           //// label.Text=Microsoft.VisualBasic.Interaction.InputBox("", "Edit Text", label.Text);

        }
        private void DragDrop(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.All;
            label.Text = e.X.ToString();
        }
    }
}
