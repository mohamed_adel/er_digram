﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ErDigram
{
    class Line
    {
        double x1,y1,x2,y2;
        public  Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer;
        public Microsoft.VisualBasic.PowerPacks.LineShape lineShape;
        public Line(double _x1, double _y1, double _x2, double _y2,MainForm F,bool total)
        {
            x1 = _x1;
            y1 = _y1;
            x2 = _x2;
            y2 = _y2;

            shapeContainer = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            lineShape = new Microsoft.VisualBasic.PowerPacks.LineShape();
            // 
            // shapeContainer1
            // 
            

            this.shapeContainer.Location = new System.Drawing.Point(0, 0);
            this.shapeContainer.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer.Name = "shapeContainer1";
            this.shapeContainer.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.lineShape});
            this.shapeContainer.Size = new System.Drawing.Size(1013, 420);
            this.shapeContainer.TabIndex = 0;
            this.shapeContainer.TabStop = false;
            // 
            // lineShape1
            // 
            this.lineShape.Name = "lineShape1";
            this.lineShape.X1 = (int)x1;
            this.lineShape.X2 = (int)x2;
            this.lineShape.Y1 = (int)y1;
            this.lineShape.Y2 = (int)y2;
            if (total)
            this.lineShape.BorderWidth = 5;
            else
            this.lineShape.BorderWidth = 2;
            F.Controls.Add(this.shapeContainer);

        }
    }
}
