﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ErDigram
{
    class RealtionConfig : Shape
    {
        public Relation parent;
        public System.Windows.Forms.Label labelFrom, labelTo, labelTitle;
        private System.Windows.Forms.ComboBox comboBoxFrom, comboBoxTo, partpicationFrom, partpicationTo;
        private System.Windows.Forms.Button btnOk, btnCancel;
        public string title;
        Drag drag;
        bool fromOne = true, toOne = true, fromTotal = true, toTotal = true;
        public RealtionConfig(MainForm F, Relation _parent)
        {

            parent = _parent;

            control = new System.Windows.Forms.Panel();
            control.Location = new System.Drawing.Point(200, 200);
            control.Size = new System.Drawing.Size(230, 150);
            control.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            control.LocationChanged += new System.EventHandler(this.LocationChanged);
            ((System.Windows.Forms.Panel)control).BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            drag = new Drag(control);


            labelTitle = new System.Windows.Forms.Label();
            labelTitle.Text = title;
            labelTitle.Location = new System.Drawing.Point(60, 0);
            labelTitle.Size = new System.Drawing.Size(100, 20);
            labelTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            labelTitle.BackColor = System.Drawing.Color.Transparent;

            comboBoxFrom = new ComboBox();
            comboBoxFrom.Items.AddRange(new object[] { "one", "many" });
            comboBoxFrom.Location = new System.Drawing.Point(10, 50);
            comboBoxFrom.Size = new System.Drawing.Size(100, 20);
            comboBoxFrom.SelectedIndex = 0;

            comboBoxTo = new ComboBox();
            comboBoxTo.Items.AddRange(new object[] { "one", "many" });
            comboBoxTo.Location = new System.Drawing.Point(120, 50);
            comboBoxTo.Size = new System.Drawing.Size(100, 20);
            comboBoxTo.SelectedIndex = 0;

            partpicationFrom = new ComboBox();
            partpicationFrom.Items.AddRange(new object[] { "Total", "Partial" });
            partpicationFrom.Location = new System.Drawing.Point(10, 80);
            partpicationFrom.Size = new System.Drawing.Size(100, 20);
            partpicationFrom.SelectedIndex = 0;

            partpicationTo = new ComboBox();
            partpicationTo.Items.AddRange(new object[] { "Total", "Partial" });
            partpicationTo.Location = new System.Drawing.Point(120, 80);
            partpicationTo.Size = new System.Drawing.Size(100, 20);
            partpicationTo.SelectedIndex = 0;


            labelFrom = new System.Windows.Forms.Label();
            labelFrom.Text = "From";
            labelFrom.Location = new System.Drawing.Point(10, 20);
            labelFrom.Size = new System.Drawing.Size(100, 20);
            labelFrom.BackColor = System.Drawing.Color.Transparent;

            labelTo = new System.Windows.Forms.Label();
            labelTo.Text = "To";
            labelTo.Location = new System.Drawing.Point(120, 20);
            labelTo.Size = new System.Drawing.Size(100, 20);
            labelTo.BackColor = System.Drawing.Color.Transparent;

            btnOk = new System.Windows.Forms.Button();
            btnOk.Text = "Ok";
            btnOk.Location = new System.Drawing.Point(60, 110);
            btnOk.Size = new System.Drawing.Size(100, 20);
            btnOk.Click += new System.EventHandler(this.OnClickOk);

            control.Controls.Add(labelTitle);
            control.Controls.Add(labelFrom);
            control.Controls.Add(labelTo);
            control.Controls.Add(comboBoxFrom);
            control.Controls.Add(comboBoxTo);
            control.Controls.Add(partpicationFrom);
            control.Controls.Add(partpicationTo);
            control.Controls.Add(btnOk);
            F.Controls.Add(control);
        }
        private void OnClickOk(object sender, EventArgs e)
        {
            fromOne = inttobool(comboBoxFrom.SelectedIndex);
            toOne = inttobool(comboBoxTo.SelectedIndex);
            fromTotal = inttobool(partpicationFrom.SelectedIndex);
            toTotal = inttobool(partpicationTo.SelectedIndex);
            this.control.Visible = false;
                for (int i = 0; i < parent.connectors.Count; i++)
                {
                    if (parent.connectors[i].from == parent)
                    {
                        parent.connectors[i].total = fromTotal;
                        parent.connectors[i].Update();
                    }
                    if (parent.connectors[i].to == parent)
                    {
                        parent.connectors[i].total = toTotal;
                        parent.connectors[i].Update();
                    }
                }
        }

        private bool inttobool(int n)
        {
            if (n == 1) return false;
            return true;
        }
    }
}
